# .zshenv is always sourced.
# Most ${ENV_VAR} variables should be saved here.
# It is loaded before .zshrc

### ENV VARS
export QT_QPA_PLATFORMTHEME="qt5ct"
export GTK2_RC_FILES="/home/tumtum/.config/gtkrc-2.0/.gtk2rc-2.0"

export BROWSER="/usr/bin/zen-browser"
export TERMINAL="/usr/bin/alacritty"
export EDITOR="/usr/bin/nvim"
export FILE="/usr/bin/ranger"
export PAGER="/bin/less"

# config
export ZDOTDIR=$HOME/.config/zsh/
export XDG_CONFIG_HOME=$HOME/.config/
export XDG_DATA_HOME=$HOME/.local/share
export XDG_STATE_HOME=$HOME/.local/state
export XDG_CACHE_HOME=$HOME/.cache
export HISTFILE="$$XDG_STATE_HOME"=$HOME/.config/zsh/history
export fpath=(~/.config/zsh/completions/ $fpath)

# Determine if we are an SSH connection
if [[ -n "$SSH_CLIENT" ]] || [[ -n "$SSH_TTY" ]]; then
    export IS_SSH=true
else
    case $(ps -o comm= -p $PPID) in
        sshd|*/sshd) IS_SSH=true
    esac
fi

# Add custom keymaps
xmodmap $HOME/.config/X11/xmodmap
xrdb -load $HOME/.config/X11/xresources

# To clean ~
export WGETRC="$XDG_CONFIG_HOME"/wgetrc
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export NVM_DIR="$XDG_DATA_HOME"/nvm
export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv
export PARALLEL_HOME="$XDG_CONFIG_HOME"/parallel
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pass
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export RXVT_SOCKET="$XDG_RUNTIME_DIR"/urxvtd
export WINEPREFIX="$XDG_DATA_HOME"/wineprefixes/default
export LESSHISTFILE=""
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export PYTHONSTARTUP=~/.config/python/pythonrc
