### ALIASES ###
alias ls='ls --color=auto'
alias grep='grep --colour=auto'
alias egrep='egrep --colour=auto'
alias fgrep='fgrep --colour=auto'

alias cp="cp -i"                          # Confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias lsn='ls --color=never'
alias v='nvim'
alias l='ls -lh'
alias la='ls -lAFh'

alias ranger='ranger_cd'
alias r='ranger_cd'
alias py='python'

# Pour les logiciels dans /opt
alias turtl='/opt/turtl/turtl'
alias zotero='/opt/zotero/zotero'
alias stremio='/opt/stremio/stremio'

# Temporaires pour les dossiers récurrents
alias cdl='cd ~/data/BOULOT/2022/LUUSE/'
alias cdb='cd ~/data/BOULOT/EN_COURS/'
alias cdu='cd /run/media/tumtum'

# Output lisible de xev
alias xevo="xev | sed -n 's/.*\(keycode .*\) (.*, \(.*\)).*/\1 → \2/p'"

# Searchable TLDR with preview window
alias tldrf='tldr --list | fzf --preview "tldr {1} --color=always" --preview-window=right,70% | xargs tldr'

alias lg='lazygit'

# Divers trucs pour ranger ~
alias yarn='yarn --use-yarnrc "$XDG_CONFIG_HOME/yarn/config"'

# Lancer nvm
alias nvm-go='source ~/.local/bin/nvm-init'

# Setup screens
alias arh='autorandr --load haut && nitrogen --restore'
alias ard='autorandr --load droite && nitrogen --restore'

# Shortcuts for lochalhost
alias l1="firefox localhost:1000"
alias l2="firefox localhost:2000"
alias l3="firefox localhost:3000"
alias l4="firefox localhost:4000"
alias l5="firefox localhost:5000"
alias l6="firefox localhost:6000"
alias l7="firefox localhost:7000"
alias l8="firefox localhost:8000"
alias l9="firefox localhost:9000"

# create a quick tmux session with name of cwd
# alias tn="tmux new -s $(pwd | sed 's/.*\///g')" → renvoie tumtum parce que aliasrc est dans le dossier home ?

# alias mousespeed='xinput --list | grep -E "HID Gaming Mouse.*pointer" | cut -f 2 | cut -d= -f 2 | parallel "xinput set-prop {} \'Coordinate Transformation Matrix\' 0.15 0 0 0 0.15 0 0 0 1"'
alias kb='setxkbmap -layout "gb,es" -variant "extd," -option grp:alt_altgr_toggle -option compose:ins -model pc105'

# fix de dog en attendant de comprendre la configuration de réseau pour utiliser
# des serveurs DNS customs
alias dog='dog @1.0.0.1'

# test du thème de couleurs
alias colortest='colortest base16'

alias firefox='zen-browser'
