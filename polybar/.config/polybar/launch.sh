#!/usr/bin/env bash

#!/bin/bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch Polybar, using default config location ~/.config/polybar/config
if type "xrandr"; then
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    MONITOR=$m polybar --log=warning mybar &
  done
else
  polybar --log=warning mybar &
fi

echo "Polybar launched..."

# # Terminate already running bar instances
# killall -q polybar
# # If all your bars have ipc enabled, you can also use
# # polybar-msg cmd quit
#
# # Launch bar1 and bar2
# echo "---" | tee -a /tmp/polybar.log
# polybar mybar 2>&1 | tee -a /tmp/polybar.log & disown
#
# echo "Bars launched..."
