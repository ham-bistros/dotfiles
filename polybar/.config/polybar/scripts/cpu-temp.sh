#!/bin/bash

raw_temp=$(sensors | grep Package | awk '{print $4}')
temp=${raw_temp:1:2}

if [ "$temp" -ge 90 ]; then
    echo "·󰈸·${raw_temp:1:6}"
elif [ "$temp" -ge 75 ]; then
    echo "·${raw_temp:1:6}"

elif [ "$temp" -ge 60 ]; then
    echo "·${raw_temp:1:6}"

elif [ "$temp" -ge 45 ]; then
    echo "·${raw_temp:1:6}"
else
    echo "·${raw_temp:1:6}"

fi
