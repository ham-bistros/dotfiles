#!/bin/sh

# Lists the IDs of mouses that have the name "HID Gaming Mouse" and uses them as
# arguments to set the mousespeed
xinput --list | grep -E "HID Gaming Mouse.*pointer" | cut -f 2 | cut -d= -f 2 | parallel "xinput set-prop {} 'Coordinate Transformation Matrix' 0.15 0 0 0 0.15 0 0 0 1"
