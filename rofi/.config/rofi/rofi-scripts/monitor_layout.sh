#!/bin/bash
# src:  https://github.com/davatorium/rofi-scripts/blob/master/monitor_layout.sh

TILES=()
TILES[0]="2e écran → haut"
TILES[1]="2e écran → droite"
TILES[2]="2e écran → gauche"
TILES[3]="2e écran → fusion"

##
#  Generate entries, where first is key.
##
function gen_entries()
{
    # le chiffre est nécessaire pour appeler la bonne commande
    for a in $(seq 0 $(( ${#TILES[@]} -1 )))
    do
        echo "${TILES[a]}"
    done
}
# Call menu, output is last field of TILES entry
SEL=$( gen_entries | rofi -dmenu -p "Monitor Setup:" -a 0 -no-custom  | awk '{print $NF}' )

# Menu output is passed on as a parameter for autorandr. Screen templates preconfigured and named manually with autorandr --save
$( autorandr --load "$SEL" && nitrogen --restore)
