vim.keymap.set("n", "<,", "<Plug>(neorg.promo.demote)", { buffer = true })
vim.keymap.set("n", "<<", "<Plug>(neorg.promo.demote.nested)", { buffer = true })
vim.keymap.set("v", "<", "<Plug>(neorg.promo.demote.range)", { buffer = true })

vim.keymap.set("n", ">,", "<Plug>(neorg.promo.promote)", { buffer = true })
vim.keymap.set("n", ">>", "<Plug>(neorg.promo.promote.nested)", { buffer = true })
vim.keymap.set("v", ">", "<Plug>(neorg.promo.promote.range)", { buffer = true })

vim.keymap.set(
	"n",
	"<LocalLeader>tc",
	"<Plug>(neorg.qol.todo-items.todo.task-cancelled)",
	{ desc = "[neorg] [T]ask [C]ancelled", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>td",
	"<Plug>(neorg.qol.todo-items.todo.task-done)",
	{ desc = "[neorg] [T]ask [D]one", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>th",
	"<Plug>(neorg.qol.todo-items.todo.task-on-hold)",
	{ desc = "[neorg] [T]ask on [H]old", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>ti",
	"<Plug>(neorg.qol.todo-items.todo.task-important)",
	{ desc = "[neorg] [T]ask [I]mportant", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>tp",
	"<Plug>(neorg.qol.todo-items.todo.task-pending)",
	{ desc = "[neorg] [T]ask [P]ending", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>tr",
	"<Plug>(neorg.qol.todo-items.todo.task-recurring)",
	{ desc = "[neorg] [T]ask [R]ecurring", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>tu",
	"<Plug>(neorg.qol.todo-items.todo.task-undone)",
	{ desc = "[neorg] [T]ask [U]ndone", buffer = true }
)

vim.keymap.set(
	"n",
	"<LocalLeader>tc",
	"<Plug>(neorg.qol.todo-items.todo.task-cancelled)",
	{ desc = "[neorg] [T]ask [C]ancelled", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>td",
	"<Plug>(neorg.qol.todo-items.todo.task-done)",
	{ desc = "[neorg] [T]ask [D]one", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>th",
	"<Plug>(neorg.qol.todo-items.todo.task-on-hold)",
	{ desc = "[neorg] [T]ask on [H]old", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>ti",
	"<Plug>(neorg.qol.todo-items.todo.task-important)",
	{ desc = "[neorg] [T]ask [I]mportant", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>tp",
	"<Plug>(neorg.qol.todo-items.todo.task-pending)",
	{ desc = "[neorg] [T]ask [P]ending", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>tr",
	"<Plug>(neorg.qol.todo-items.todo.task-recurring)",
	{ desc = "[neorg] [T]ask [R]ecurring", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>tu",
	"<Plug>(neorg.qol.todo-items.todo.task-undone)",
	{ desc = "[neorg] [T]ask [U]ndone", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>ta",
	"<Plug>(neorg.qol.todo-items.todo.task-ambiguous)",
	{ desc = "[neorg] [T]ask [A]mbiguous", buffer = true }
)

vim.keymap.set(
	"n",
	"<LocalLeader>id",
	"<Plug>(neorg.tempus.insert-date)",
	{ desc = "[neorg] [I]nsert [D]ate", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>li",
	"<Plug>(neorg.pivot.list.invert)",
	{ desc = "[neorg] [L]ist [I]nvert", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>lt",
	"<Plug>(neorg.pivot.list.toggle)",
	{ desc = "[neorg] [L]ist [T]oggle", buffer = true }
)
vim.keymap.set(
	"n",
	"<LocalLeader>cm",
	"<Plug>(neorg.looking-glass.magnify-code-block)",
	{ desc = "[neorg] [C]ode [M]agnify", buffer = true }
)
