local map = function(mode, lhs, rhs, opts)
	local options = { noremap = true, silent = true }
	if opts then
		options = vim.tbl_extend("force", options, opts)
	end
	vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

-- [[ Basic Keymaps ]]

-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
vim.keymap.set({ "n", "v" }, "<Space>", "<Nop>", { silent = true })

-- Remap for dealing with word wrap
vim.keymap.set("n", "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set("n", "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Delete current buffer
map("n", "<leader>q", ":bdelete<CR>")

-- Cnext and Cprev for quickfix list
map("n", "[q", ":cprev<CR>", { desc = "Previous [Q]uickfix item" })
map("n", "]q", ":cnext<CR>", { desc = "Next [Q]uickfix item" })

-- Quits window
map("n", "<leader>Q", ":q!<CR>")

-- Change buffers (previous/next)
map("n", "[b", ":bp<CR>")
map("v", "[b", ":bp<CR>")

map("n", "]b", ":bn<CR>")
map("v", "]b", ":bn<CR>")

-- Replace current by empty line
map("n", "<leader>c", '"acc<ESC>')

-- Go to last buffer
map("n", "<leader>l", ":b#<CR>")

-- Quick save
map("", "<leader>w", ":w!<cr>")
map("", "<leader>W", ":wall<cr>")

-- Fix n and N, keep cursor in the center (pas sûr)
map("n", "n", "nzzzv")
map("n", "N", "Nzzzv")

-- Paste in place without registering what is selected
map("x", "<leader>p", '"_dP')

-- Inserts blank line below/above
map("n", "<leader>o", "o<ESC>")
map("n", "<leader>O", "O<ESC>")

-- Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,
-- which is the default
map("", "Y", "y$")

-- Make a copy to another register than x (here it's register a), so you can
-- change and replace by previously yanked text
map("n", "c", '"ac')
map("v", "c", '"ac')

map("n", "C", '"aC')
map("n", "cc", '"acc')

-- Move to the beginning of the line, even if they are wrapped
map("", "H", "^")
map("", "gH", "g^")

-- Move to the end of the line even if they are wrapped
map("", "L", "$")
map("", "gL", "g$")

-- Map Ctrl + Del to delete the next word in insert mode
map("i", "<C-Del>", "<esc>lcw")

-- Maps for easy indentation
map("i", "<S-Tab>", "<Esc><<i")
map("v", ">", ">gv")
map("v", "<", "<gv")

-- Maps for moving things in visual mode
map("v", "J", ":m '>+1<CR>gv=gv")
map("v", "K", ":m '<-2<CR>gv=gv")

-- Maps to center screen when goinf up and down half a page
-- map("n", "<C-d>", "<C-d>zz")
-- map("n", "<C-u>", "<C-u>zz")

-- Map to switch to another project form nvim with tmux and fzf
-- map("n", "<C-f>", "<cmd>silent !tmux tmux new tmux-sessionizer<CR>")

-- Quick replace of the word under cursor
map("n", "<leader><F2>", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

-- unmap Q, I guess
map("n", "Q", "<nop>")

if vim.lsp.inlay_hint then
	vim.keymap.set("n", "<leader>hh", function()
		vim.lsp.inlay_hint(0, nil)
	end, { desc = "Toggle Inlay [H]ints" })
end

-- Diagnostic keymaps
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, { desc = "Go to previous diagnostic message" })
vim.keymap.set("n", "]d", vim.diagnostic.goto_next, { desc = "Go to next diagnostic message" })
vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float, { desc = "Open floating diagnostic message" })
vim.keymap.set("n", "<leader>/q", vim.diagnostic.setloclist, { desc = "Open diagnostics list" })
