return {
	"NvChad/nvim-colorizer.lua",
	config = function()
		require("colorizer").setup {
			filetypes = {
				'css',
				'scss',
				'sass',
				'less',
				'javascript',
				'typescript',
				'html',
				'htmldjango',
			},
			user_default_options = { css = true },
			mode = "virtualtext",
			virtualtext = "■",
			sass = { enable = true, parsers = { "css" }, },
			tailwind = true,
		}
	end,
}
