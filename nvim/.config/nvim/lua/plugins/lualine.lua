return {
	-- Set lualine as statusline
	"nvim-lualine/lualine.nvim",
	dependencies = { "nvim-tree/nvim-web-devicons" },
	-- See `:help lualine.txt`
	config = function()
		local lualine = require("lualine")
		local lazy_status = require("lazy.status")

		lualine.setup({
			options = {
				icons_enabled = true,
				theme = "auto",
				section_separators = "",
				component_separators = "",
			},
			globalstatus = true,
			sections = {
				lualine_c = {
					{
						"filename",
						newfile_status = true,
						path = 3,
						shorting_target = 30,
						symbols = {
							modified = "•",
							readonly = "⚿",
						},
					},
				},
				lualine_x = {
					{
						lazy_status.updates,
						cond = lazy_status.has_updates,
					},
					{ "encoding" },
					{
						"filetype",
						colored = true,
					},
					{ "fileformat" },
				},
			},
			tabline = {
				-- lualine_a = {},
				-- lualine_b = {},
				-- lualine_c = {},
				-- lualine_x = {},
				-- lualine_y = {},
				-- lualine_z = {},
			},
			extensions = { "fugitive", "quickfix" },
		})
	end,
}
