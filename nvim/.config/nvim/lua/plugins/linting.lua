return {
	"mfussenegger/nvim-lint",
	event = {
		"BufWritePre",
		"BufNewFile",
	},
	config = function()
		local lint = require("lint")

		lint.linters_by_ft = {
			markdown = { "markdownlint" },
			php = { "phpcs" },
			twig = { "curlylint" },
			jinja = { "djlint", "curlylint" },
			django = { "djlint", "curlylint" },
			python = { "flake8" },
			json = { "jsonlint" },
			lua = { "selene" },
			shell = { "shellcheck" },
			sql = { "sqlfluff" },
			css = { "stylelint" },
			scss = { "stylelint" },
			yaml = { "yamllint" },
			javascript = { "eslint_d" },
			typescript = { "eslint_d" },
			javascriptreact = { "eslint_d" },
			typescriptreact = { "eslint_d" },
		}

		local phpcs = require("lint").linters.phpcs
		phpcs.args = {
			"-q",
			"--standard=PSR2",
			"--report=json",
			"-",
		}

		local lint_augroup = vim.api.nvim_create_augroup("lint", { clear = true })

		vim.api.nvim_create_autocmd({ "BufEnter", "BufWritePost", "InsertLeave" }, {
			group = lint_augroup,
			callback = function()
				lint.try_lint()
			end,
		})
	end,
}
