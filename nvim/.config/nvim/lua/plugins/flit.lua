return {
	-- LSP Configuration & Plugins
	'ggandor/flit.nvim',
	dependencies = {
		'tpope/vim-repeat',
		{
			'ggandor/leap.nvim',
			opts = {
				special_keys = {
					repeat_search = '<enter>',
					next_phase_one_target = '<,nter>',
					next_target = { '<enter>', '-' },
					prev_target = { '<tab>', ',' },
					next_group = '<space>',
					prev_group = '<tab>',
					multi_accept = '<enter>',
					multi_revert = '<backspace>',
				}
			}
		},
	},
	config = function()
		require('flit').setup {
			keys = { f = 'f', F = 'F', t = 't', T = 'T' },
			-- A string like "nv", "nvo", "o", etc.
			labeled_modes = "v",
			multiline = true,
			opts = {}
		}
	end
}
