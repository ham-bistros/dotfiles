return {
	"nvim-neorg/neorg",
	dependencies = { "luarocks.nvim" },
	lazy = false, -- Disable lazy loading as some `lazy.nvim` distributions set `lazy = true` by default
	version = "*", -- Pin Neorg to the latest stable release
	config = function()
		require("neorg").setup({
			load = {
				["core.defaults"] = {}, -- Loads default behaviour
				["core.keybinds"] = {}, -- Keybinds configured in after/ftplugins/norg.lua to avoid conflicts
				["core.concealer"] = {}, -- Adds pretty icons to your documents
				["core.itero"] = {}, -- Alt+Enter to continue lists and todos
				["core.completion"] = {
					config = {
						engine = "nvim-cmp",
					},
				}, -- Autocompletion module
				["core.pivot"] = {}, -- Switch ordered/unordered lists
				["core.promo"] = {}, -- Promote/demote headings
				["core.export"] = {}, -- Adds export capabilities
				["core.export.markdown"] = {}, -- Interface for markdown export specifically
				["core.dirman"] = { -- Manages Neorg workspaces
					config = {
						workspaces = {
							todo = "~/data/kDrive/todo",
							ideas = "~/data/kDrive/ideas",
						},
						default_workspaces = "todo",
					},
				},
				["core.text-objects"] = {},
			},
		})

		-- vim.keymap.set("n", "<up>", "<Plug>(neorg.text-objects.item-up)", {})
		-- vim.keymap.set("n", "<down>", "<Plug>(neorg.text-objects.item-down)", {})
		vim.keymap.set({ "v", "o", "x" }, "iH", "<Plug>(neorg.text-objects.textobject.heading.inner)", {})
		vim.keymap.set({ "v", "o", "x" }, "aH", "<Plug>(neorg.text-objects.textobject.heading.outer)", {})
	end,
}
