return {
	-- LSP Configuration & Plugins
	'ggandor/leap.nvim',
	config = function()
		require('leap').add_default_mappings()
		require('leap').opts.special_keys.next_target = { '<enter>', '-' }
		require('leap').opts.special_keys.prev_target = { '<tab>', ',' }
	end
}
