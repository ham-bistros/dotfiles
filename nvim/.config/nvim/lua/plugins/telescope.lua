-- Fuzzy Finder (files, lsp, etc)
return {
	"nvim-telescope/telescope.nvim",
	dependencies = { "nvim-lua/plenary.nvim", { "nvim-telescope/telescope-fzf-native.nvim", build = "make" } },
	config = function()
		-- Enable telescope fzf native, if installed
		pcall(require("telescope").load_extension, "fzf")

		-- See `:help telescope.builtin`
		vim.keymap.set(
			"n",
			"<leader>so",
			require("telescope.builtin").oldfiles,
			{ desc = "[so] Find recently opened files" }
		)
		vim.keymap.set(
			"n",
			"<leader><space>",
			require("telescope.builtin").buffers,
			{ desc = "[SPACE] Find existing buffers" }
		)
		vim.keymap.set("n", "<leader>/", function()
			-- You can pass additional configuration to telescope to change theme, layout, etc.
			require("telescope.builtin").current_buffer_fuzzy_find(require("telescope.themes").get_dropdown({
				winblend = 10,
				previewer = false,
			}))
		end, { desc = "[/] Fuzzily search in current buffer" })

		vim.keymap.set(
			"n",
			"<leader>sp",
			require("telescope.builtin").builtin,
			{ desc = "[S]earch Builtin Telescope [P]ickers" }
		)

		vim.keymap.set("n", "<leader>sf", require("telescope.builtin").find_files, { desc = "[S]earch [F]iles" })
		vim.keymap.set("n", "<leader>sh", require("telescope.builtin").help_tags, { desc = "[S]earch [H]elp" })
		vim.keymap.set(
			"n",
			"<leader>sw",
			require("telescope.builtin").lsp_dynamic_workspace_symbols,
			{ desc = "[S]earch LSP dynamic [W]orkspace symbols" }
		)
		-- Remplacé par une fonction custom (lua/custom/telescope/multigrep.lua)
		-- vim.keymap.set("n", "<leader>sg", require("telescope.builtin").live_grep, { desc = "[S]earch by [G]rep" })
		vim.keymap.set("n", "<leader>sd", require("telescope.builtin").diagnostics, { desc = "[S]earch [D]iagnostics" })
		vim.keymap.set(
			"n",
			"<leader>sc",
			require("telescope.builtin").commands,
			{ desc = "[S]earch telescope [C]ommands" }
		)

		-- Load custom telescope pickers
		require("custom.telescope.multigrep").setup()
	end,
}
