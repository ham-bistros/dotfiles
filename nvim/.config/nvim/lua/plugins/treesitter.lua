return {
	-- Highlight, edit, and navigate code
	"nvim-treesitter/nvim-treesitter",
	event = { "BufReadPre", "BufNewFile" },
	dependencies = {
		"nvim-treesitter/nvim-treesitter-textobjects",
		"nvim-treesitter/nvim-treesitter-context",
		"JoosepAlviste/nvim-ts-context-commentstring",
		"windwp/nvim-ts-autotag",
	},
	config = function()
		pcall(require("nvim-treesitter.install").update({ with_sync = true }))

		-- [[ Telescope setup and configuration ]]
		require("telescope").setup({
			defaults = {
				mappings = {
					i = {
						["<C-u>"] = false,
						["<C-d>"] = false,
						["<C-h>"] = "which_key",
					},
				},
				file_ignore_patterns = {
					"%.JPEG",
					"%.JPG",
					"%.PNG",
					"%.TIF",
					"%.TIFF",
					"%.avif",
					"%.chk",
					"%.eot",
					"%.gif",
					"%.ico",
					"%.jpeg",
					"%.jpg",
					"%.kra",
					"%.mp3",
					"%.mp4",
					"%.otf",
					"%.pdf",
					"%.png",
					"%.psd",
					"%.pyc",
					"%.tif",
					"%.tiff",
					"%.ttf",
					"%.wav",
					"%.pbf",
					"%.webm",
					"%.webp",
					"%.woff",
					"%.woff2",
					"%~",
					"__pycache__/",
					"node_modules",
					"venv",
					".venv",
				},
			},
			extensions = {
				fzf = {},
			},
			pickers = {
				find_files = {
					theme = "ivy",
				},
			},
		})

		require("ts_context_commentstring").setup({
			enable_autocmd = false,
		})

		require("Comment").setup({
			pre_hook = require("ts_context_commentstring.integrations.comment_nvim").create_pre_hook(),
		})

		--Treesitter repeat moves (should be in treesitter config function)
		local repeat_move = require("nvim-treesitter.textobjects.repeatable_move")
		-- vim way: ; goes to the direction you were moving.

		vim.keymap.set({ "n", "x", "o" }, ";", repeat_move.repeat_last_move)
		vim.keymap.set({ "n", "x", "o" }, ",", repeat_move.repeat_last_move_opposite)
	end,
}
