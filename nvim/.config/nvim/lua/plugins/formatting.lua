return {
	-- Formatting and linting made easier (in theory)
	"stevearc/conform.nvim",
	event = { "BufReadPre", "BufNewFile" },
	-- Builtin configuration, optional
	config = function()
		local conform = require("conform")

		conform.setup({
			formatters_by_ft = {
				php = { "php_cs_fixer" },
				twig = { "djlint" },
				jinja = { "djlint" },
				django = { "djlint" },
				python = { "isort", "black" },
				lua = { "stylua" },
				shell = { "shfmt" },
				sql = { "sql_formatter" },
				css = { "prettier" },
				json = { "prettier" },
				scss = { "prettier" },
				html = { "prettier" },
				javascript = { "prettier" },
				typescript = { "prettier" },
				javascriptreact = { "prettier" },
				typescriptreact = { "prettier" },
				markdown = { "prettier" },
				yaml = { "yamlfmt" },
				xml = { "xmlformatter" },
				-- ["_"] = { "trim_whitespace" },
			},
			notify_on_error = true,
			format_on_save = {
				lsp_fallback = true,
				async = false,
				timeout_ms = 3000,
			},
		})

		conform.formatters.php = {
			prepend_args = function(ctx)
				return { "--config", "~/.config/phpcsfixer/config.php" }
			end,
		}

		conform.formatters.djlint = {
			prepend_args = function(ctx)
				return { "--configuration", "/home/tumtum/.config/djlint/djlintrc" }
			end,
		}

		-- conform.formatters.prettier = {
		-- 	prepend_args = function(ctx)
		-- 		return { "", "" }
		-- 	end,
		-- }

		-- Set this value to true to silence errors when formatting a block fails
		require("conform.formatters.injected").options.ignore_errors = false

		vim.keymap.set({ "n", "v" }, "<leader>F", function()
			conform.format({
				lsp_fallback = true,
				async = false,
				timeout_ms = 3000,
			})
		end, { desc = "[F]ormat file or range (in visual mode)" })
	end,
}
