return {
	"zenbones-theme/zenbones.nvim",
	priority = 1000,
	lazy = false,
	dependencies = "rktjmp/lush.nvim",
	config = function()
		-- require("zenbones").setup()
		vim.o.background = "light"
		vim.g.zenbones_darken_comments = 45
		vim.g.solid_vert_split = true

		-- light theme options
		vim.g.lightness = "bright"
		vim.g.darken_noncurrent_window = true

		-- dark theme options
		vim.g.darkness = "stark"
		vim.g.lighten_noncurrent_window = true

		vim.cmd.colorscheme("zenwritten")
	end,
}

-- return {
-- 	"EdenEast/nightfox.nvim",
-- 	priority = 1000,
-- 	lazy = false,
-- 	config = function()
-- 		require("nightfox").setup({
-- 			options = {
-- 				inverse = {
-- 					match_paren = true,
-- 					visual = false,
-- 					seach = false,
-- 				},
-- 			},
-- 		})
-- 		vim.cmd.colorscheme("terafox")
-- 	end,
-- }

-- good ol' gruvbox
-- return {
-- "ellisonleao/gruvbox.nvim",
-- priority = 1000,
-- config = function()
-- 	require("gruvbox").setup({
-- 		undercurl = true,
-- 		underline = true,
-- 		bold = true,
-- 		italic = {
-- 			strings = false,
-- 			comments = true,
-- 			operators = false,
-- 			folds = true,
-- 		},
-- 		strikethrough = true,
-- 		invert_selection = false,
-- 		invert_signs = false,
-- 		invert_tabline = false,
-- 		invert_intend_guides = false,
-- 		inverse = true, -- invert background for search, diffs, statuslines and errors
-- 		contrast = "", -- can be "hard", "soft" or empty string
-- 		palette_overrides = {},
-- 		dim_inactive = false,
-- 		transparent_mode = false,
-- 		overrides = {
-- 			WinSeparator = { fg = "#fbf1c7" }, -- corresponds to light0 in the palette
-- 		},
-- 	})
-- 	vim.o.background = "dark"
-- 	vim.cmd.colorscheme("gruvbox")
-- 	end,
-- }
