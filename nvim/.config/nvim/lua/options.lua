-- [[ Setting options ]]
-- See `:help vim.o`

-- Ignore compiled files
vim.opt.wildignore = { "__pycache__", "*.o", "*~", "*.pyc", "*pycache*" }

-- Set highlight on search
vim.o.hlsearch = true
vim.o.incsearch = true
--
-- Cool floating window popup menu for completion on command line
vim.opt.pumblend = 17
vim.opt.wildmode = "longest:full"
vim.opt.wildoptions = "pum"

-- Make line numbers default
vim.wo.number = true
vim.wo.numberwidth = 2
vim.opt.relativenumber = true
vim.opt.cursorline = true

vim.opt.scrolloff = 8
vim.opt.expandtab = false
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4

-- Something with wrapped lines
vim.opt.breakindent = true
vim.opt.showbreak = ">  "
vim.opt.linebreak = true

-- Don't show vim mode under lualine
vim.opt.showmode = false
vim.opt.showmatch = true

-- Not sure but something to do with the effects of the commands showing in the buffer or in a floating window
vim.opt.inccommand = "split"

-- Windows can't change all the time when opening/closing splits
vim.opt.equalalways = false

-- Enable mouse mode
vim.o.mouse = "a"

-- Enable break indent
vim.o.breakindent = true

-- Case insensitive searching UNLESS /C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- Decrease update time
vim.o.updatetime = 250
vim.o.timeout = true
vim.o.timeoutlen = 300

-- Keep signcolumn on by default
vim.wo.signcolumn = "yes"

-- Undo and backup options
vim.o.backup = false
vim.o.writebackup = false
vim.o.undofile = true
-- vim.o.swapfile = false

-- Global statusline for every windows
vim.o.laststatus = 3

-- Better buffer splitting
vim.opt.splitright = true
vim.opt.splitbelow = true

-- Add invisible characters
vim.o.list = true
vim.o.listchars = "lead: ,trail:·,nbsp:◇,tab:→ ,extends:→,precedes:←,"

vim.opt.formatoptions = "ncj1prq"
vim.opt.formatoptions:remove({ "a", "t", "2", "o" })
-- 	+ "q" -- Allow formatting of comments with gq
-- 	- "a" -- Auto formatting is BAD.
-- 	- "t" -- Don't auto format my code. I got linters for that.
-- 	+ "c" -- In general, I like it when comments respect textwidth
-- 	+ "q" -- Allow formatting comments w/ gq
-- 	- "o" -- O and o, don't continue comments
-- 	+ "r" -- But do continue when pressing enter.
-- 	+ "n" -- Indent past the formatlistpat, not underneath it.
-- 	+ "j" -- Auto-remove comments if possible.
-- 	- "2" -- I'm not in gradeschool anymore
-- 	+ "p" -- Don't break after ". " like in Mr. {break}Feynman

-- /!\ certaines options sont overwritten par des ftplugins de morts, seule solution une autocmd ???

vim.bo.textwidth = 80

-- Makes neovim and host OS clipboard play nicely with each other
vim.o.clipboard = "unnamedplus"

-- Set colorscheme
vim.o.termguicolors = true

-- Set completeopt to have a better completion experience
vim.o.completeopt = "menu,menuone,noselect"

vim.o.conceallevel = 3
