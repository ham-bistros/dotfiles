#!/usr/bin/bash

cd /home/tumtum/.config/tmux/ || exit

if ! tmux has-session -t TMUX;  then
    tmux new -s TMUX -d
    tmux rename-window -t TMUX:1 conf
    tmux send-keys -t TMUX:1 "nvim tmux.conf" Enter
fi

tmux a -t TMUX
